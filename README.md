# Selenium scraping bot
The bot for scraping site contents by script based on selenium module. 
Supported browsers: google chrome, firefox, opera, phantomjs
Supported virtual display for non-graphical call mode 

## run sample
` python bot.py <script.xml> [params list] `

## install components
### selemium, lxml, virtual display
  
`sudo pip install -r requirements.txt`

**chromedriver** version not less than 2.31! 
from `https://sites.google.com/a/chromium.org/chromedriver/downloads` install to `/usr/local/bin`

## xml script tags:

**bot** - root tag

**site** - scan site params (name, url, waitfor, title, list)

**base** - set base marker for further use '.' in xpath (name, by, c)

**for** - cycle for (name, max) 

**foreach** - cycle for group operation with list of the elements

**if** - condition for execute or pass subtags

**scroll** - scroll long page (pages, repeat)

**nav** - navigation action (change page / or page options)

**collect** - collect data from site to common plane list

**col** - data column descriptor for collect tag


## run examples

` python bot.py instagramm.xml test`



## updates:

### aug-17-2018
  added script tags for, scroll 

### aug-25-2017
  added proxy rotation from list each selenium call 

### aug-24-2017
  added data report name style as "site-alias_time-stamp_filter1_filterN.json"
  updated proxy/chrome 

### aug-23-2017
  added site pages caching by ttl="minutes" param in script tags: site, foreach/jump

### aug-22-2017
  added proxy support
  added to script call param filters for filtration site pages that need scraping

### aug-21-2017
  first site scraping bot version
