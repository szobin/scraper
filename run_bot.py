#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import logging

logFormatter = logging.Formatter("[%(levelname)-5.5s] %(name)s %(message)s")
rootLogger = logging.getLogger()

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)
rootLogger.setLevel(logging.INFO)


if __name__ == '__main__':
    from lib.bot_lib import SeleniumBot

    base_dir = os.path.dirname(os.path.abspath(__file__))
    bot = SeleniumBot(base_dir, sys.argv)
    try:
        if bot.errors == 0: 
            bot.execute()
    finally:
        bot.finalize() 