#!/usr/bin/env python
from lxml import etree
import os
import time
import json
import random
from lxml import html
from selenium import webdriver
from selenium.webdriver.chrome import options
from selenium.webdriver.firefox import options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.proxy import *
from lib.xml_lib import *
from lib import url_lib
from lib import time_lib
import logging


__version__ = "1.5"
logger = logging.getLogger(__name__)

SCROLL_PAUSE_TIME = 0.5


class SeleniumBot:
    
    def __init__(self, base_dir, params, config_fn='bot_config.xml'):
        self.start_time = time_lib.now()
        self.BASE_DIR = base_dir
        self.params = list(params)
        self.errors = 0
        self.config_fn = config_fn
        self.config = None
        self.driver = None

        self.proxy_mode = ""
        self.proxy_host = ""
        self.proxy_port = ""
        self.proxy_username = ""
        self.proxy_password = ""

        self.base_url = None
        self.list_name = None
        self.report_list = None
        self.report_col_values = []

        self.script = None
        if base_dir is None:
            return

        if len(params) < 2:
            self.log_msg("SeleniunBot usage: >python runbot.py config.xml [params]")
            self.script_fn = 'bot_script.xml'
        else:
            self.script_fn = self.params[1]

        self.load_config()
        try:
            self.browser_type = xml_str_param(self.config, "browser", 'opera')
            self.driver_path = xml_str_param(self.config, "driver_path")
            self.browser_path = xml_str_param(self.config, "browser_path")
            self.cache_path = os.path.join(self.BASE_DIR, xml_str_param(self.config, "cache_path", "cache"))
            self.data_path = os.path.join(self.BASE_DIR, xml_str_param(self.config, "data_path", "data"))

            self.load_proxy_params()

            # virtual display
            self.display = xml_str_param(self.config, "display")
            self.virtual_display = None
            if len(self.display) > 0:
                display_params = list(map(int, self.display.split(',')))
                from pyvirtualdisplay import Display
                self.virtual_display = Display(visible=display_params[0], size=(display_params[1], display_params[2]))
                self.virtual_display.start()

            # self.webdriver_service = service.Service(self.grabber_path)
            # self.webdriver_service.start()
        except Exception as ex:
            self.log_msg(" error: webdriver was not started: " + str(ex), True)

        self.load_script()

        if self.errors == 0:
            self.log_msg("SeleniunBot version %s was started OK" % __version__)

        return

    def log_msg(self, msg, is_error=False):
        if is_error:
            self.errors += 1
            logger.error(msg)
        else:
            logger.info(msg)

    def load_config(self):
        if not os.path.exists(self.config_fn):
            self.config = etree.Element("config", grabber="selenium")
            return
        try:
            f = open(self.config_fn, 'r')
            try:
                xmlstr = f.read()
            finally:
                f.close()
            self.config = etree.fromstring(xmlstr)
        except IOError as ex:
            self.config = etree.Element("config", grabber="selenium")
            self.log_msg(" error: config file not loaded: "+str(ex), True)
            return
        except Exception as ex:
            self.config = etree.Element("config", grabber="selenium")
            self.log_msg("error: config has wrong format: "+str(ex), True)
            return
           
    def load_script(self):
        if not os.path.exists(self.script_fn):
            self.log_msg(" error: bot script file not found: "+self.script_fn, True)
            return

        self.script = etree.Element("bot")
        try:
            f = open(self.script_fn, 'r')
            try:
                xmlstr = f.read()
            finally:
                f.close()
            self.script = etree.fromstring(xmlstr)
        except IOError as ex:
            self.log_msg(" error: config file not loaded: "+str(ex), True)
            return
            
        except Exception as ex:
            self.log_msg("error: config has wrong format: "+str(ex), True)
            return

    def load_proxy_params(self):
        self.proxy_host = ""
        self.proxy_port = ""
        self.proxy_username = ""
        self.proxy_password = ""
        try:
            proxy_tag = xml_search_tag(self.config, "proxies")
            if proxy_tag is None:
                self.proxy_host = xml_str_param(self.config, "proxy_host")
                self.proxy_port = xml_str_param(self.config, "proxy_port", "80")
                self.proxy_username = xml_str_param(self.config, "proxy_username")
                self.proxy_password = xml_str_param(self.config, "proxy_passw")
                return

            self.proxy_mode = xml_str_param(self.config, "proxy")
            if len(self.proxy_mode) == 0:
                return

            if self.proxy_mode == "random":
                n = random.randint(0, len(proxy_tag) - 1)
                tag = proxy_tag[n]
                self.proxy_host = xml_str_param(tag, "host")
                self.proxy_port = xml_str_param(tag, "port", "80")
                self.proxy_username = xml_str_param(tag, "username")
                self.proxy_password = xml_str_param(tag, "passw")
                return

            n = int(self.proxy_mode)
            tag = proxy_tag[n]
            self.proxy_host = xml_str_param(tag, "host")
            self.proxy_port = xml_str_param(tag, "port", "80")
            self.proxy_username = xml_str_param(tag, "username")
            self.proxy_password = xml_str_param(tag, "passw")
            return
        except Exception as ex:
            self.log_msg("error load proxy params: "+str(ex))

    def finalize(self):
        time_delta = time_lib.now() - self.start_time
        self.log_msg('time: '+str(time_delta))

    def execute(self):
        try:
            for site_tag in self.script:
                self.do_process_site_tag(site_tag) 
        except Exception as ex:
            self.log_msg("execute error: "+str(ex))

    def get_driver_proxy(self):
        proxy = None
        if len(self.proxy_host) > 0:
            proxy = Proxy({
                'proxyType': ProxyType.MANUAL,
                'httpProxy': self.proxy_host+':'+self.proxy_port,
                'ftpProxy': self.proxy_host+':'+self.proxy_port,
                'sslProxy': self.proxy_host+':'+self.proxy_port,
                'socksProxy': self.proxy_host+':'+self.proxy_port,
                'noProxy': ''})

            if len(self.proxy_username) > 0:
                proxy.socks_username = self.proxy_username
                if len(self.proxy_password) > 0:
                    proxy.socks_password = self.proxy_password
        return proxy

    def do_update_proxy_browser_caps(self, _caps):
        proxy_str = "%s:%s@%s:%s" % (self.proxy_username, self.proxy_password, self.proxy_host, self.proxy_port)
        _caps['proxy'] = {'proxyType': 'MANUAL',
                          'httpProxy': proxy_str,
                          'ftpProxy': proxy_str,
                          'sslProxy': proxy_str,
                          'noProxy': '',
                          'class': "org.openqa.selenium.Proxy",
                          'autodetect': False}
        if len(self.proxy_username) > 0:
            _caps['proxy']['socksUsername'] = self.proxy_username
            _caps['proxy']['socksPassword'] = self.proxy_password

    def do_update_proxy_options(self, _opts):
        _opts.set_preference("network.proxy.type", 1)
        _opts.set_preference("network.proxy.http", self.proxy_host)
        _opts.set_preference("network.proxy.http_port", int(self.proxy_port))
        _opts.set_preference("network.proxy.ssl", self.proxy_host)
        _opts.set_preference("network.proxy.ssl_port", int(self.proxy_port))
        _opts.set_preference("network.proxy.ftp", self.proxy_host)
        _opts.set_preference("network.proxy.ftp_port", int(self.proxy_port))
        _opts.set_preference("network.proxy.socks", self.proxy_host)
        _opts.set_preference("network.proxy.socks_port", int(self.proxy_port))
        if len(self.proxy_username) > 0:
            _opts.set_preference("network.proxy.socks_username", self.proxy_username)
            _opts.set_preference("network.proxy.socks_password", self.proxy_password)

    def do_init_driver(self):
        _prefs = {
            "profile.managed_default_content_settings.images": 2,
            "plugins.plugins_disabled": ["Adobe Flash Player", "Shockwave Flash"]
        }

        browser_name = self.browser_type

        _browserDriverLoc = os.path.abspath(self.driver_path)
        if len(self.browser_path) > 0:
            _browserExeLoc = os.path.abspath(self.browser_path)
        else:
            _browserExeLoc = ''

        if self.browser_type == 'phantomjs':
            self.driver = webdriver.PhantomJS(executable_path=_browserDriverLoc)
            self.driver.set_window_size(1280, 720)

        elif self.browser_type == 'opera':
            _browserOpts = webdriver.chrome.options.Options()
            if len(_browserExeLoc) > 0:
                _browserOpts._binary_location = _browserExeLoc
            _browserOpts.add_argument('--start-maximized')
            _browserOpts.add_experimental_option("prefs", _prefs)

            _browserCaps = webdriver.DesiredCapabilities.OPERA.copy()
            self.driver = webdriver.Opera(executable_path=_browserDriverLoc,
                                          # opera_options=_browserOpts,
                                          desired_capabilities=_browserCaps)
        elif self.browser_type == 'firefox':
            _browserOpts = webdriver.firefox.options.Options()
            if len(_browserExeLoc) > 0:
                _browserOpts._binary_location = _browserExeLoc
            _browserOpts.add_argument('--start-maximized')
            for pk in _prefs:
                _browserOpts.set_preference(pk, str(_prefs[pk]))

            _browserCaps = webdriver.DesiredCapabilities.FIREFOX.copy()
            # if proxy is not None:
            #    proxy.add_to_capabilities(_browserCaps)

            if len(self.proxy_host) > 0:
                # _browserOpts.proxy = proxy
                self.do_update_proxy_options(_browserOpts)

            self.driver = webdriver.Firefox(executable_path=_browserDriverLoc,
                                            firefox_options=_browserOpts,
                                            capabilities=_browserCaps
                                            )

        elif self.browser_type == 'chrome':
            # proxy = self.get_driver_proxy()
            _browserOpts = webdriver.chrome.options.Options()
            if len(_browserExeLoc) > 0:
                _browserOpts._binary_location = _browserExeLoc
            _browserOpts.add_argument('--start-maximized')
            _browserOpts.add_experimental_option("prefs", _prefs)

            _browserCaps = webdriver.DesiredCapabilities.CHROME.copy()
            # if proxy is not None:
            #   proxy.add_to_capabilities(_browserCaps)
            #    alternative call method
            #    _browserOpts.add_argument('--proxy-server=http://%s:%s@%s:%s' % (self.proxy_username,
            #       self.proxy_password, self.proxy_host, self.proxy_port)
            # --proxy-server=http://user:password@proxy.com:8080

            if len(self.proxy_host) > 0:
                self.do_update_proxy_browser_caps(_browserCaps)

            self.driver = webdriver.Chrome(executable_path=_browserDriverLoc,
                                           chrome_options=_browserOpts,
                                           desired_capabilities=_browserCaps)
        else:
            raise Exception("config error: unsupported selenium browser type: " + browser_name)

        # alternative call method
        # driver = webdriver.Remote(self.webdriver_service.service_url, browser_dc)

        s = " - init browser %s" % browser_name
        if len(self.proxy_host) > 0:
            s += " proxy to: %s:%s" % (self.proxy_host, self.proxy_port)
        self.log_msg(s)
        return 0

    def url_to_cache_filename(self, url):
        if not os.path.exists(self.cache_path):
            os.makedirs(self.cache_path)
        fn = os.path.join(self.cache_path, url_lib.url_to_filename(url)+'.cache')
        return fn

    def report_name_to_data_filename(self, report_name):
        if not os.path.exists(self.data_path):
            os.makedirs(self.data_path)
        fn = os.path.join(self.data_path, report_name+'.json')
        return fn

    def do_store_cache(self, url):
        try:
            page_html = self.driver.page_source
            page_tree = html.fromstring(page_html)
            fn = self.url_to_cache_filename(url)
            is_need_remove = 0
            try:
                f = open(fn, 'wb')
                is_need_remove = 1
                try:
                    f.write(page_html.encode('utf-8'))
                finally:
                    f.close()
            except Exception as exc:
                self.log_msg("error store cache, url: %s, message: %s" % (url, repr(exc)))
                if is_need_remove > 0:
                    os.remove(fn)
            return page_tree
        except Exception as exc:
            self.log_msg("error store cache, url: %s, message: %s" % (url, repr(exc)))

    def do_load_cache(self, url):
        try:
            fn = self.url_to_cache_filename(url)
            f = open(fn, 'rb')
            try:
                page_html = f.read().decode('utf-8')
            finally:
                f.close()
            page_tree = html.fromstring(page_html)
            return page_tree
        except Exception as ex:
            raise Exception('error load cache, url: %s, message: %s' % (url, str(ex)))

    def do_load_url(self, tag, url):
        ttl = xml_int_param(tag, 'ttl', 0)
        if ttl <= 0:
            try:
                self.driver.get(url)
            except Exception as ex:
                raise Exception('error load url %s, message: %s' % (url, str(ex)))
            self.do_driver_wait_for(tag)
            return None

        cache_fn = self.url_to_cache_filename(url)
        cache_time = time_lib.time_delta_in_minutes(time_lib.now(), time_lib.file_time(cache_fn))
        if cache_time > ttl:
            try:
                self.driver.get(url)
            except Exception as ex:
                raise Exception('error load url %s, message: %s' % (url, str(ex)))
            if self.do_driver_wait_for(tag):
                return self.do_store_cache(url)
            else:
                raise Exception("wrong page content")

        return self.do_load_cache(url)

    def do_process_site_tag(self, site_tag):
        site_name = xml_str_param(site_tag, 'name', 'NA')
        stage = 'init'
        try:
            report_name = xml_str_param(site_tag, 'report_name', site_name)
            site_url = xml_str_param(site_tag, 'url')
            self.base_url = url_lib.get_base_url(site_url)

            site_debug = xml_int_param(site_tag, 'debug', 0)
            is_date_in_report_name = xml_int_param(site_tag, 'use_date', 1)
            is_filter_in_report_name = xml_int_param(site_tag, 'use_filter', 1)

            if is_date_in_report_name == 1:
                report_name = time_lib.add_time_stamp_to_name(report_name)
            if is_filter_in_report_name == 1:
                for i in range(2, len(self.params)):
                    report_name += '_'+url_lib.url_proc_param(self.params[i], "-")

            if site_url == "":
                self.log_msg("error: site tag has no url attribute in bot script", True)
                return

            title = xml_str_param(site_tag, 'title', 'Unknown')
            self.base_url = url_lib.get_base_url(site_url)
            self.list_name = xml_str_param(site_tag, 'list', 'list')
            self.report_list = list()
            self.report_col_values = list()

            self.log_msg("Scrap info from site: %s" % site_name)

            report_table = dict(title=title, date=time_lib.datetime_str(time_lib.now()),
                                site=(site_name, site_url))

            stage = 'init_driver'
            self.do_init_driver()

            try:
                try:
                    stage = 'load_url'
                    root_element = self.do_load_url(site_tag, site_url)

                    stage = 'process_acts'
                    self.do_process_actions_tag(root_element, site_tag)
                    # self.driver.close()  # for firefox - lead to crash
                finally:
                    self.store_report_table(report_name, report_table)
            finally:
                if site_debug == 0:
                    self.driver.quit()

        except Exception as ex:
            msg = str(ex)
            self.log_msg("error scrap info on stage %s from site: %s, message: %s" % (stage, site_name, msg))

    def store_report_table(self, report_name, report_table):
        try:
            if len(self.report_list) == 0:
                self.log_msg('result was not stored (nothing to store)')
                return False

            if self.list_name is not None:
                report_table.update({self.list_name: self.report_list})
            json_str = json.dumps(report_table, indent=4, ensure_ascii=False)
            fn = self.report_name_to_data_filename(report_name)
            f = open(fn, "w")
            try:
                f.write(json_str)
                self.log_msg('result was stored to: ' + fn+' items collected: '+str(len(self.report_list)))
                return True
            finally:
                f.close()
        except Exception as ex:
            self.log_msg("store report error: "+str(ex))

    def get_driver_element(self, b, c):
        if b == 'xpath':
            nav = self.driver.find_element_by_xpath(c)
        elif b == 'title':
            nav = self.driver.find_element_by_link_text(c)
        elif b == 'ptitle':
            nav = self.driver.find_element_by_partial_link_text(c)
        else:
            nav = None
        return nav

    def do_driver_wait_for(self, act_tag, element=None):
        if element is not None:
            return True
        w = xml_str_param(act_tag, 'waitfor')
        wto = xml_int_param(act_tag, 'wait_to', 10)
        k = xml_str_param(act_tag, 'expect_kind', 'presence')

        if len(w) > 0:
            try:
                if k == 'visibility':
                    WebDriverWait(self.driver, wto).until(ec.visibility_of_element_located((By.XPATH, w)))
                else:
                    WebDriverWait(self.driver, wto).until(ec.presence_of_element_located((By.XPATH, w)))
            except TimeoutException:
                self.log_msg('waitfor timeout: ' + str(wto))
                return False
            except Exception as ex:
                self.log_msg('waitfor error: ' + str(ex))
                return False
        return True

    def do_process_actions_tag(self, cur_element, parent_tag):
        for act_tag in parent_tag:
            act_name = act_tag.tag
            if act_name == 'nav':
                self.do_site_nav(act_tag)
            if act_name == 'scroll':
                self.do_site_scroll(act_tag)
            elif act_name == 'base':
                self.do_site_base(cur_element, act_tag)
            elif act_name == 'sleep':
                self.do_site_sleep(cur_element, act_tag)
            elif act_name == 'for':
                r = self.do_site_for(cur_element, act_tag)
                if not r:
                    return False
            elif act_name == 'foreach':
                r = self.do_site_for_each(cur_element, act_tag)
                if not r:
                    return False
            elif act_name == 'if':
                self.do_site_if(cur_element, act_tag)
            elif act_name == 'delay':
                t = xml_int_param(act_tag, 't', 5)
                self.log_msg('delay: '+str(t))
                time.sleep(t)
            elif act_name == 'collect':
                r = self.do_site_collect(act_tag)
                if not r:
                    return False
            elif act_name == 'break':
                self.log_msg('break')
                return False
            else:
                self.log_msg('warning: unknown action tag: '+act_name)
                return False
        return True

    def do_site_scroll(self, act_tag):
        try:
            pages = int(xml_str_param(act_tag, 'pages', '0'))
            need_store_cahe = int(xml_str_param(act_tag, 'cache_html', '0'))
            repeat_no = int(xml_str_param(act_tag, 'repeat', '4'))
            n = pages

            # Get scroll height
            last_height = self.driver.execute_script("return document.body.scrollHeight")

            while True:
                # Scroll down to bottom
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                n_check = repeat_no
                while True:
                    # Wait to load page
                    time.sleep(SCROLL_PAUSE_TIME)

                    # Calculate new scroll height and compare with last scroll height
                    new_height = self.driver.execute_script("return document.body.scrollHeight")
                    if new_height > last_height:
                        last_height = new_height
                        break
                    n_check -= 1
                    if n_check <= 0:
                        last_height = 0
                        break

                if last_height == 0:
                    break

                n -= 1
                if n == 0:
                    break

            if n > 0:
                    self.log_msg("site scroll finish reached: pg:{}/{}".format(n, pages))
            else:
                self.log_msg("site scrolled ok: pages:{}".format(pages))

            time.sleep(SCROLL_PAUSE_TIME)
            if need_store_cahe == 1:
                self.do_store_cache(self.base_url)
        except Exception as ex:
            self.log_msg("site scroll error: %s" % (repr(ex)))

    def do_site_nav(self, act_tag):
        n = xml_str_param(act_tag, 'name')
        t = ''
        try:
            c = xml_str_param(act_tag, 'c')
            h = xml_str_param(act_tag, 'h')
            by = xml_str_param(act_tag, 'by', 'xpath')
            a = xml_str_param(act_tag, 'action', 'click')
            nav = None
            hidden_nav = None
            if len(c) > 0:
                nav = self.get_driver_element(by, c)
                # nav = self.driver.find_element_by_xpath(c)
            if len(h) > 0:
                hidden_nav = self.get_driver_element(by, h)

            if a == 'click':
                if nav is not None:
                    try:
                        t = nav.text
                    except Exception as exc:
                        logger.exception(repr(exc))
                    nav.click()
            if a == 'perform':
                if nav is not None:
                    try:
                        t = nav.text
                    except Exception as exc:
                        logger.exception(repr(exc))
                    ac = ActionChains(self.driver)
                    if hidden_nav is None:
                        ac.move_to_element(nav).click()
                    else:
                        ac.move_to_element(nav).click(hidden_nav)
                    ac.perform()
            if a == 'jump':
                if nav is not None:
                    jump_url = nav.get_attribute('href')
                    self.driver.get(jump_url)
            if a == 'wait':
                pass

            if not self.do_driver_wait_for(act_tag):
                return

            if len(n) > 0:
                self.log_msg('nav: '+n+'/'+a+' OK')
        except Exception as ex:
            self.log_msg("site nav (%s:%s) error: %s" % (n, t, str(ex)))

    def do_site_sleep(self, cur_element, act_tag):
        name = xml_str_param(act_tag, 'name', 'sleep')
        s = int(xml_str_param(act_tag, 'n', '60'))
        self.log_msg("{} site sleep: {} secs".format(name, s))
        time.sleep(s)

    def do_site_base(self, cur_element, act_tag):
        r = True
        name = xml_str_param(act_tag, 'name')
        try:
            c = xml_str_param(act_tag, 'c')
            # by = xml_str_param(act_tag, 'by', 'xpath')
            if cur_element is None:
                page_html = self.driver.page_source
                page_tree = html.fromstring(page_html)
            else:
                page_tree = cur_element

            nav_list = page_tree.xpath(c)
            if len(nav_list) == 0:
                raise Exception('base: not found')

            self.log_msg('set base: %s OK: %s' % (name, str(len(nav_list))))
            for nav in nav_list:
                self.do_process_actions_tag(nav, act_tag)

        except Exception as ex:
            self.log_msg("set base %s error: %s" % (name, str(ex)))

        return r

    def expand_value(self, value_str):
        if not value_str:
            return None

        s = value_str.find("{{arg.")
        while s >= 0:
            s2 = value_str.find("}}", s)
            if s2 < 0:
                value_str = value_str[:s] + value_str[s+6:]
                continue
            n = value_str[s+6:s2]
            v = url_lib.url_proc_param(self.params[int(n)])

            value_str = value_str[:s] + v + value_str[s2+2:]
            s = value_str.find("{{arg.")
        return value_str

    def do_site_for_jumps(self, act_tag, tag_list, header_list=None):
        r = True
        name = xml_str_param(act_tag, 'name')
        filter_param = xml_str_param(act_tag, 'filter')
        filter_value = self.expand_value(filter_param)

        nn = 0
        links = []
        i_ref = 0
        for tag in tag_list:
            if header_list is None:
                hd = tag.text
            else:
                hd = header_list[i_ref].text
            if filter_value is not None:
                if hd.count(filter_value) == 0:
                    i_ref += 1
                    continue

            u = tag.get('href')
            links.append([u, hd])
            i_ref += 1

        if len(links) == 0:
            raise Exception("filter: `%s` was not passed any link titles" % filter_value)

        self.log_msg('foreach: %s started: links found: %s' % (name, str(len(links))))

        self.report_col_values.append("")
        for link in links:
            self.report_col_values[len(self.report_col_values) - 1] = link[1]
            link_url = url_lib.update_url(self.base_url, link[0])
            root_element = self.do_load_url(act_tag, link_url)
            nn += 1
            r = self.do_process_actions_tag(root_element, act_tag)
            if not r:
                break

        self.report_col_values.pop()
        self.log_msg('foreach: ' + name + '/jumps finished OK: ' + str(nn))
        return r

    def do_site_for_navs(self, act_tag, nav_list):
        r = True
        name = xml_str_param(act_tag, 'name')
        nn = 0
        for nav in nav_list:
            r = self.do_process_actions_tag(nav, act_tag)
            nn += 1
            if not r:
                break

        self.log_msg('foreach: ' + name + '/jumps OK: ' + str(nn))
        return r

    def do_site_for(self, cur_element, act_tag):
        r = True
        name = xml_str_param(act_tag, 'name')
        nn_max = int(xml_str_param(act_tag, 'max', "100"))
        nn = 0
        try:
            while True:
                nn += 1
                self.log_msg("for cycle {} #{}/{}".format(name, nn, nn_max))
                r = self.do_process_actions_tag(cur_element, act_tag)
                if not r:
                    break
                if nn >= nn_max:
                    break
        except Exception as ex:
            self.log_msg("site for: %s error: %s" % (name, str(ex)))
        return r

    def do_site_for_each(self, cur_element, act_tag):
        r = True
        name = xml_str_param(act_tag, 'name')
        a = xml_str_param(act_tag, 'action', 'jumps')

        if cur_element is None:
            page_html = self.driver.page_source
            page_tree = html.fromstring(page_html)
        else:
            page_tree = cur_element

        try:
            h = xml_str_param(act_tag, 'h')
            data_header = None
            if len(h) > 0:
                header_list = page_tree.xpath(h)
                if len(header_list) > 0:
                    data_header = header_list[0]

            if data_header is not None:
                t = data_header.text
                self.report_col_values.append(t)

            selector = xml_str_param(act_tag, 'c')
            header_selector = xml_str_param(act_tag, 'hc')
            header_list = None
            if len(selector) > 0:
                nav_list = page_tree.xpath(selector)
                if len(nav_list) == 0:
                    raise Exception("empty list for selector: %s" % selector)

                if len(header_selector) > 0:
                    header_list = page_tree.xpath(header_selector)
                if a == 'jumps':
                    r = self.do_site_for_jumps(act_tag, nav_list, header_list)
                elif a == 'navs':
                    r = self.do_site_for_navs(act_tag, nav_list)

            if data_header is not None:
                self.report_col_values.pop()

        except Exception as ex:
            self.log_msg("site foreach: %s/%s error: %s" % (name, a, str(ex)))
        return r

    def do_site_if(self, cur_element, act_tag):
        try:
            selector = xml_str_param(act_tag, 'c')
            try:
                nav = self.driver.find_element_by_xpath(selector)
                if nav is not None:
                    self.log_msg("site if: was found: OK")
                    self.do_process_actions_tag(cur_element, act_tag)
            except Exception as exc:
                self.log_msg("site if: selector was not found: {} error: {}".format(selector, repr(exc)))

        except Exception as exc:
            self.log_msg("site if error: " + repr(exc), True)

    def do_site_collect(self, act_tag):
        r = True
        try:
            check_add = int(xml_str_param(act_tag, 'add', '0'))
            cols = []
            for col_tag in act_tag:
                c = xml_str_param(col_tag, 'c')
                col_data = self.driver.find_elements_by_xpath(c)
                cols.append(col_data)

            i_row = 0
            col1 = cols[0]
            for d in col1:
                v = xml_str_param(act_tag[0], 'v')

                if v == "":
                    dd = d.text.strip()
                    if len(dd) == 0:
                        continue
                else:
                    dd = d.get_attribute(v)

                ef = xml_str_param(act_tag[0], 'exfilter')
                if len(ef) > 0:
                    if dd.count(ef) > 0:
                        i_row += 1
                        continue

                fl = xml_int_param(act_tag[0], 'first_line_only', 0)
                if fl == 1:
                    dd1 = dd.split('\n')
                    dd = dd1[0]

                sf = xml_int_param(act_tag[0], 'skip_first', 0)
                if sf > 0:
                    dd1 = dd.split(' ')
                    while sf > 0:
                        if len(dd1) > 1:
                            del dd1[0]
                        sf -= 1
                    dd = ' '.join(dd1)

                d_row = list(self.report_col_values)
                d_row.append(dd)
                for i_col in range(1, len(cols)):
                    try:
                        c = cols[i_col]
                        d2 = c[i_row].text
                    except Exception as exc:
                        logger.exception(repr(exc))
                        d2 = ''
                    d_row.append(d2.strip())
                if check_add:
                    if not (d_row in self.report_list):
                        self.report_list.append(d_row)
                        i_row += 1
                else:
                    self.report_list.append(d_row)
                    i_row += 1

            if check_add and i_row == 0:
                r = False
            self.log_msg("collected items OK: {}/{}".format(i_row, len(self.report_list)))
            return r
        except Exception as ex:
            self.log_msg("site collect error: "+str(ex))
            return False
